package tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.project.persistence.enums.Currency;
import com.project.web.pojo.request.CancelPaymentRequest;
import com.project.web.pojo.request.CreateAccountRequest;
import com.project.web.pojo.request.CreateUserRequest;
import com.project.web.pojo.request.MakePaymentRequest;
import com.project.web.pojo.response.IdResponse;
import com.project.web.pojo.response.PaymentResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static com.project.persistence.enums.PaymentType.*;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

public class PaymentControllerTest extends AbstractTest {

    @Test
    public void makePaymentTestBadRequest() throws JsonProcessingException {
        // empty invalid request
        MakePaymentRequest req = new MakePaymentRequest(null, null, null, null,
                null, null, null, null, null);
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(makePayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // account not found
        req = new MakePaymentRequest(12423L, 3464L, BigDecimal.valueOf(100), EUR_PAYMENT,
                Currency.EUR, "12345686", "153436", "34654", "test");
        entity = produceEntity(req);
        response = template.postForEntity(makePayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertTrue(Objects.requireNonNull(response.getBody()).contains("No account found for user id 12423 " +
                "and account id 3464."));


        entity = produceEntity(new CreateUserRequest("Test1", "Test2",
                LocalDate.now().minusYears(20), 1279608867L));
        response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse userId = mapper.readValue(response.getBody(), IdResponse.class);

        entity = produceEntity(new CreateAccountRequest(userId.getId(), BigDecimal.valueOf(1000)));
        response = template.postForEntity(addUserAccountUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse accId = mapper.readValue(response.getBody(), IdResponse.class);

        // negative balance
        req = new MakePaymentRequest(userId.getId(), accId.getId(), BigDecimal.valueOf(-100), EUR_PAYMENT,
                Currency.EUR, "12345686", "153436", "34654", "test");
        entity = produceEntity(req);
        response = template.postForEntity(makePayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // wrong currency
        req = new MakePaymentRequest(userId.getId(), accId.getId(), BigDecimal.valueOf(100), EUR_PAYMENT,
                Currency.USD, "12345686", "153436", "34654", "test");
        entity = produceEntity(req);
        response = template.postForEntity(makePayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertTrue(Objects.requireNonNull(response.getBody()).contains("EUR_PAYMENT has to have EUR currency."));

        // empty details
        req = new MakePaymentRequest(userId.getId(), accId.getId(), BigDecimal.valueOf(100), EUR_PAYMENT,
                Currency.EUR, "12345686", "153436", "34654", null);
        entity = produceEntity(req);
        response = template.postForEntity(makePayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertTrue(Objects.requireNonNull(response.getBody()).contains("For EUR_PAYMENT details field has to be provided."));
    }

    @Test
    public void makeCancelPaymentTestSuccess() throws JsonProcessingException {
        HttpEntity<String> entity = produceEntity(new CreateUserRequest("Test1", "Test2",
                LocalDate.now().minusYears(20), 12380867L));
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse userId = mapper.readValue(response.getBody(), IdResponse.class);

        entity = produceEntity(new CreateAccountRequest(userId.getId(), BigDecimal.valueOf(1000)));
        response = template.postForEntity(addUserAccountUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse accId = mapper.readValue(response.getBody(), IdResponse.class);

        entity = produceEntity(new MakePaymentRequest(userId.getId(), accId.getId(), BigDecimal.valueOf(100), USD_PAYMENT,
                Currency.USD, "12345686", "153436", "34654", "test"));
        response = template.postForEntity(makePayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse paymentId = mapper.readValue(response.getBody(), IdResponse.class);

        // select
        response = template.getForEntity(getPayment + "?paymentId=" + paymentId.getId(), String.class);
        PaymentResponse resp = mapper.readValue(response.getBody(), PaymentResponse.class);
        assertEquals(paymentId.getId(), resp.getId());
        assertEquals("100.00", resp.getAmount().toString());
        assertEquals("153436", resp.getCreditorIban());
        assertEquals("12345686", resp.getDebitorIban());
        assertEquals(null, resp.getBicCode());
        assertNull(resp.getCancelled());
        assertNotNull(resp.getNotified());
        assertNull(resp.getCancellationFee());

        // select all
        TypeReference<List<String>> typeReference = new TypeReference<>() {};
        response = template.getForEntity(listPayments + "?offset=0&limit=500&minAmount=10", String.class);
        List<String> resp2 = mapper.readValue(response.getBody(), typeReference);
        String idFound = resp2.stream()
                .filter(id -> id.equals(paymentId.getId().toString()))
                .findAny()
                .orElse(null);

        assertNotNull(idFound);

        // cancel payment
        entity = produceEntity(new CancelPaymentRequest(accId.getId(), paymentId.getId()));
        response = template.postForEntity(cancelPayment, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        // select
        response = template.getForEntity(getPayment + "?paymentId=" + paymentId.getId(), String.class);
        resp = mapper.readValue(response.getBody(), PaymentResponse.class);
        assertEquals(paymentId.getId(), resp.getId());
        assertEquals("100.00", resp.getAmount().toString());
        assertEquals("153436", resp.getCreditorIban());
        assertEquals("12345686", resp.getDebitorIban());
        assertEquals(null, resp.getBicCode());
        assertNotNull(resp.getCancelled());
        assertEquals("0.00", resp.getCancellationFee().toString()); // same hour cancellation

        // select all
        response = template.getForEntity(listPayments + "?offset=0&limit=500&minAmount=10", String.class);
        resp2 = mapper.readValue(response.getBody(), typeReference);
        idFound = resp2.stream()
                .filter(id -> id.equals(paymentId.getId().toString()))
                .findAny()
                .orElse(null);

        assertNull(idFound);
    }
}
