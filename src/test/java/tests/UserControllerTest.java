package tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.web.pojo.request.AddBalanceRequest;
import com.project.web.pojo.request.CreateAccountRequest;
import com.project.web.pojo.request.CreateUserRequest;
import com.project.web.pojo.response.IdResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class UserControllerTest extends AbstractTest {

    @Test
    public void addUserTestBadRequest() throws JsonProcessingException {
        // empty invalid request
        CreateUserRequest req = new CreateUserRequest(null, null, null, null);
        HttpEntity<String> entity = produceEntity(req);
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // non-unique id
        req = new CreateUserRequest("Test1", "Test2", LocalDate.now().minusYears(20), 12345L);
        entity = produceEntity(req);
        response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        req = new CreateUserRequest("Test1", "Test2", LocalDate.now().minusYears(20), 12345L);
        entity = produceEntity(req);
        response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addAccountTestBadRequest() throws JsonProcessingException {
        // empty invalid request
        HttpEntity<String> entity = produceEntity(new CreateUserRequest("Test1", "Test2",
                LocalDate.now().minusYears(20), 1234567L));
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse userId = mapper.readValue(response.getBody(), IdResponse.class);

        // negative balance
        entity = produceEntity(new CreateAccountRequest(userId.getId(), BigDecimal.valueOf(-700)));
        response = template.postForEntity(addUserAccountUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // no such user
        entity = produceEntity(new CreateAccountRequest(123142L, BigDecimal.valueOf(-700)));
        response = template.postForEntity(addUserAccountUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addAccountTestAddBalance() throws JsonProcessingException {
        // empty invalid request
        HttpEntity<String> entity = produceEntity(new CreateUserRequest("Test1", "Test2",
                LocalDate.now().minusYears(30), 1256567L));
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse userId = mapper.readValue(response.getBody(), IdResponse.class);

        entity = produceEntity(new CreateAccountRequest(userId.getId(), BigDecimal.valueOf(100)));
        response = template.postForEntity(addUserAccountUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        IdResponse accId = mapper.readValue(response.getBody(), IdResponse.class);

        // select
        response = template.getForEntity(getBalanceUrl + "?userId=" + userId.getId() + "&accountId=" + accId.getId(), String.class);
        BigDecimal resp = mapper.readValue(response.getBody(), BigDecimal.class);
        assertEquals("100.00", resp.toString());

        // no such account
        entity = produceEntity(new AddBalanceRequest(userId.getId(), 123533L, BigDecimal.valueOf(400)));
        response = template.postForEntity(addBalanceUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        //success
        entity = produceEntity(new AddBalanceRequest(userId.getId(), accId.getId(), BigDecimal.valueOf(400)));
        response = template.postForEntity(addBalanceUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(getBalanceUrl + "?userId=" + userId.getId() + "&accountId=" + accId.getId(), String.class);
        resp = mapper.readValue(response.getBody(), BigDecimal.class);
        assertEquals("500.00", resp.toString());
    }
}
