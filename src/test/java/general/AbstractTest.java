package general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.Application;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class AbstractTest {

    @LocalServerPort
    private int port;

    protected ObjectMapper mapper;

    protected String addUserUrl;
    protected String addUserAccountUrl;
    protected String addBalanceUrl;
    protected String getBalanceUrl;
    protected String makePayment;
    protected String cancelPayment;
    protected String getPayment;
    protected String listPayments;

    @Autowired
    protected TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        URL base = new URL("http://localhost:" + port + "/");
        addUserUrl = base.toString() + "user/create-user";
        addUserAccountUrl = base.toString() + "user/add-account";
        addBalanceUrl = base.toString() + "user/add-balance";
        getBalanceUrl = base.toString() + "user/get-balance";
        makePayment = base.toString() + "payment/make-payment";
        cancelPayment = base.toString() + "payment/cancel-payment";
        getPayment = base.toString() + "payment/get-payment";
        listPayments = base.toString() + "payment/list-payments";
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        //mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
    }

    protected HttpEntity<String> produceEntity(Object riskRequest) throws JsonProcessingException {
        String json = mapper.writeValueAsString(riskRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }
}
