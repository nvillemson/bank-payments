package com.project.web.pojo.request;

import com.project.persistence.enums.Currency;
import com.project.persistence.enums.PaymentType;
import com.project.web.validation.EnumNamePattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MakePaymentRequest {

    @NotNull
    private Long userId;

    @NotNull
    private Long accountId;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal amount;

    @NotNull
    @EnumNamePattern(regexp = "EUR_PAYMENT|USD_PAYMENT|BOTH_PAYMENT")
    private PaymentType paymentType;

    @NotNull
    @EnumNamePattern(regexp = "EUR|USD")
    private Currency currency;

    @NotNull
    @Size(min = 1, max = 50)
    private String debitorIban;

    @NotNull
    @Size(min = 1, max = 50)
    private String creditorIban;

    @Size(min = 1, max = 50)
    private String bicCode;

    @Size(min = 1, max = 50)
    private String details;
}
