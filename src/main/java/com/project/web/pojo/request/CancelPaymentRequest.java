package com.project.web.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CancelPaymentRequest {

    @NotNull
    private Long accountId;

    @NotNull
    private Long paymentId;
}
