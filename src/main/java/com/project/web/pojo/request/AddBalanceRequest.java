package com.project.web.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddBalanceRequest {

    @NotNull
    private Long userId;

    @NotNull
    private Long accountId;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal balance;

}
