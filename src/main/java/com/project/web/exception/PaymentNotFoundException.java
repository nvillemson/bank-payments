package com.project.web.exception;

public class PaymentNotFoundException extends RuntimeException {

    private String message;

    public PaymentNotFoundException(String message) {
        super(message);
    }
}
