package com.project.web.exception;

public class InsufficientFundsException extends RuntimeException {

    private String message;

    public InsufficientFundsException(String message) {
        super(message);
    }
}
