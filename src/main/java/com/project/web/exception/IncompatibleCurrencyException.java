package com.project.web.exception;

public class IncompatibleCurrencyException extends RuntimeException {

    private String message;

    public IncompatibleCurrencyException(String message) {
        super(message);
    }

}
