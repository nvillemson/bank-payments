package com.project.web.exception;

public class MissingFieldException extends RuntimeException {

    private String message;

    public MissingFieldException(String message) {
        super(message);
    }
}
