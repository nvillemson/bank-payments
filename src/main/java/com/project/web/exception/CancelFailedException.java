package com.project.web.exception;

public class CancelFailedException extends RuntimeException {

    private String message;

    public CancelFailedException(String message) {
        super(message);
    }
}
