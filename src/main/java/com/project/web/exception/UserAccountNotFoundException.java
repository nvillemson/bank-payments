package com.project.web.exception;

public class UserAccountNotFoundException extends RuntimeException {

    private String message;

    public UserAccountNotFoundException(String message) {
        super(message);
    }
}
