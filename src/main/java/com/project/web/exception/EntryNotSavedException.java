package com.project.web.exception;

public class EntryNotSavedException extends RuntimeException {

    private String message;

    public EntryNotSavedException(String message) {
        super(message);
    }
}
