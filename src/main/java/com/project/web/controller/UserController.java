package com.project.web.controller;

import com.project.web.pojo.request.AddBalanceRequest;
import com.project.web.pojo.request.CreateAccountRequest;
import com.project.web.pojo.request.CreateUserRequest;
import com.project.web.pojo.response.IdResponse;
import com.project.web.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;

/**
 * deleting user and account is out of scope for this test assignment
 * combination of userId and accountId for better protection against possible frauds (defensive code)
 * both are ids and indexed, should not add anything to query complexity
 */
@SuppressWarnings("unused")
@Controller
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @PostMapping("/create-user")
    public @ResponseBody
    IdResponse createUser(@RequestBody @Valid CreateUserRequest req) {
        return userService.createUser(req.getFirstName(), req.getLastName(), req.getPersonalId(), req.getBirthDate());
    }

    @PostMapping("/add-account")
    public @ResponseBody
    IdResponse addAccount(@RequestBody @Valid CreateAccountRequest req) {
        return userService.createAccount(req.getUserId(), req.getBalance());
    }

    @PostMapping("/add-balance")
    @ResponseStatus(HttpStatus.OK)
    public void addBalance(@RequestBody @Valid AddBalanceRequest req) {
        userService.addBalance(req.getUserId(), req.getAccountId(), req.getBalance());
    }

    @GetMapping("/get-balance")
    public @ResponseBody
    BigDecimal getBalance(@RequestParam("userId") Long userId, @RequestParam("accountId") Long accountId) {
        return userService.getBalance(userId, accountId);
    }
}
