package com.project.web.controller;

import com.project.web.pojo.request.MakePaymentRequest;
import com.project.web.pojo.request.CancelPaymentRequest;
import com.project.web.pojo.response.IdResponse;
import com.project.web.pojo.response.PaymentResponse;
import com.project.web.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

@SuppressWarnings("unused")
@Controller
@AllArgsConstructor
@RequestMapping("/payment")
public class PaymentController {

    private final PaymentService paymentService;

    @PostMapping("/make-payment")
    public @ResponseBody IdResponse makePayment(@RequestBody @Valid MakePaymentRequest req) {
        return paymentService.makePayment(req.getUserId(), req.getAccountId(), req.getAmount(), req.getPaymentType(),
                req.getCurrency(), req.getDebitorIban(), req.getCreditorIban(), req.getBicCode(), req.getDetails());
    }

    @PostMapping("/cancel-payment")
    public @ResponseBody IdResponse cancelPayment(@RequestBody @Valid CancelPaymentRequest req) {
        return paymentService.cancelPayment(req.getAccountId(), req.getPaymentId());
    }

    // get payment by id
    @GetMapping("/get-payment")
    public @ResponseBody PaymentResponse getPayment(@RequestParam("paymentId") Long paymentId) {
        return paymentService.getPayment(paymentId);
    }

    // get payments (not cancelled, filtered by amount)
    @GetMapping("/list-payments")
    public @ResponseBody List<String> listPayments(@RequestParam("offset") Long offset, @RequestParam("limit") Long limit,
                                                   @RequestParam("minAmount") BigDecimal minAmount) {
        return paymentService.listPayments(offset, limit, minAmount);
    }

}
