package com.project.web.service;

import com.project.persistence.model.Account;
import com.project.persistence.model.User;
import com.project.persistence.repository.AccountCrudRepository;
import com.project.persistence.repository.UserCrudRepository;
import com.project.web.exception.EntryNotSavedException;
import com.project.web.exception.UserAccountNotFoundException;
import com.project.web.exception.UserNotFoundException;
import com.project.web.pojo.response.IdResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@AllArgsConstructor
public class UserService {

    private final UserCrudRepository userCrudRepository;
    private final AccountCrudRepository accountCrudRepository;

    /**
     *
     * @param firstName user first name
     * @param lastName user last name
     * @param personalId user personal id
     * @param birthDate user date of birth
     * @return created user id
     */
    public IdResponse createUser(String firstName, String lastName, Long personalId, LocalDate birthDate) {
        try {
            User saved = userCrudRepository.save(new User(null, firstName, lastName, birthDate, personalId,
                    LocalDateTime.now(), new ArrayList<>()));
            return new IdResponse(saved.getId());
        } catch (Exception ex) {
            throw new EntryNotSavedException(ex.getMessage());
        }
    }

    /**
     * some functionality is dropped like Account should have an account name (needs some sort of auto-generation)
     * @param userId user who owns the account
     * @param balance initial balance
     * @return account id
     */
    public IdResponse createAccount(Long userId, BigDecimal balance) {
        Optional<User> user = userCrudRepository.findById(userId);
        if (user.isEmpty()) {
            throw new UserNotFoundException("No user with id " + userId +".");
        }
        Account saved = accountCrudRepository.save(new Account(null, balance, LocalDateTime.now(),
                user.get(), new ArrayList<>()));
        return new IdResponse(saved.getId());
    }

    /**
     * some of the moments will be simplified as technically it has to be a registered credit transaction
     * for testing purposes we only check that account exists and belongs to correct user and increase balance
     * in the means of test assignment we only need to make payments so this part is simplified
     * @param userId user who owns the account
     * @param accountId account that needs balance change
     * @param balance balance increase sum
     */
    public void addBalance(Long userId, Long accountId, BigDecimal balance) {
        int i = accountCrudRepository.increaseAccountBalance(userId, accountId, balance);
        if (i == 0) {
            throw new UserAccountNotFoundException("Could not update balance as no entry found " +
                    "for user id " + userId +" and account id " + accountId + ".");
        }
    }

    public BigDecimal getBalance(Long userId, Long accountId) {
        return accountCrudRepository.findAccountBalance(userId, accountId);
    }
}
