package com.project.web.service;

import com.project.persistence.enums.Currency;
import com.project.persistence.enums.PaymentType;
import com.project.persistence.model.Account;
import com.project.persistence.model.Payment;
import com.project.persistence.repository.AccountCrudRepository;
import com.project.persistence.repository.PaymentCrudRepository;
import com.project.web.exception.*;
import com.project.web.pojo.response.IdResponse;
import com.project.web.pojo.response.PaymentResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.*;

@Service
@AllArgsConstructor
public class PaymentService {

    private final AccountCrudRepository accountCrudRepository;
    private final PaymentCrudRepository paymentCrudRepository;
    private final RestTemplate restTemplate;

    /**
     *
     * @param userId user
     * @param accountId user account
     * @param amount transaction amount
     * @param paymentType BOTH_PAYMENT, EUR_PAYMENT or USD_PAYMENT
     * @param currency payment currency
     * @param debitorIban debitor Iban
     * @param creditorIban creditor Iban
     * @param bicCode present for BOTH_PAYMENT paymentType
     * @param details present for EUR_PAYMENT and nullable for USD_PAYMENT paymentType
     * @return successful transaction id
     */
    public IdResponse makePayment(Long userId, Long accountId, BigDecimal amount, PaymentType paymentType, Currency currency,
                                  String debitorIban, String creditorIban, String bicCode, String details) {
        Optional<Account> account = accountCrudRepository.findAccount(userId, accountId);
        if (account.isEmpty()) {
            throw new UserAccountNotFoundException("No account found " +
                    "for user id " + userId +" and account id " + accountId + ".");
        }

        Account actualAccount = account.get();
        if (actualAccount.getBalance().compareTo(amount) < 0) {
            throw new InsufficientFundsException("Actual account balance " + actualAccount.getBalance().toString() +
                    " smaller than transaction amount " + amount.toString() + ".");
        }

        Payment newPayment = validatePaymentType(paymentType, currency, bicCode, details);

        newPayment.setAmount(amount);
        newPayment.setAccount(actualAccount);
        newPayment.setCurrency(currency);
        newPayment.setDebitorIban(debitorIban);
        newPayment.setCreditorIban(creditorIban);
        newPayment.setType(paymentType);
        newPayment.setStarted(LocalDateTime.now());

        newPayment = paymentCrudRepository.save(newPayment);
        return new IdResponse(newPayment.getId());
    }

    public IdResponse cancelPayment(Long accountId, Long paymentId) {
        Optional<Payment> payment = paymentCrudRepository.findPayment(accountId, paymentId);
        if (payment.isEmpty()) {
            throw new PaymentNotFoundException("No payment found for accountId " + accountId + " and userId " + accountId + ".");
        }

        Payment actualPayment = payment.get();
        if (!isSameDay(actualPayment.getStarted(), LocalDateTime.now())) {
            throw new CancelFailedException("Cannot cancel transaction because not today transaction.");
        }

        int hoursDiff = LocalDateTime.now().getHour() - actualPayment.getStarted().getHour();
        BigDecimal cancellationFee = actualPayment.getType().getCoefficient().multiply(BigDecimal.valueOf(hoursDiff));

        actualPayment.setCancelled(LocalDateTime.now());
        actualPayment.setCancellationFee(cancellationFee);

        paymentCrudRepository.save(actualPayment); // update payment
        accountCrudRepository.increaseAccountBalance(accountId, actualPayment.getAmount().subtract(cancellationFee));
        return new IdResponse(actualPayment.getId());
    }

    public PaymentResponse getPayment(Long paymentId) {
        Optional<Payment> payment = paymentCrudRepository.findById(paymentId);
        if (payment.isEmpty()) {
            throw new PaymentNotFoundException("No payment found for paymentId " + paymentId + ".");
        }
        Payment actualPayment = payment.get();
        return new PaymentResponse(actualPayment.getId(), actualPayment.getAmount(), actualPayment.getType().name(),
                actualPayment.getCurrency().name(), actualPayment.getDebitorIban(), actualPayment.getCreditorIban(), actualPayment.getBicCode(),
                actualPayment.getDetails(), actualPayment.getStarted(), actualPayment.getCancelled(),
                actualPayment.getCancellationFee(), actualPayment.getNotified());
    }

    public List<String> listPayments(Long offset, Long limit, BigDecimal minAmount) {
        return paymentCrudRepository.findAllPaymentIds(offset, limit, minAmount);
    }

    private static boolean isSameDay(LocalDateTime timestamp, LocalDateTime timestampToCompare) {
        return timestamp.truncatedTo(DAYS).isEqual(timestampToCompare.truncatedTo(DAYS));
    }

    private Payment validatePaymentType(PaymentType paymentType, Currency currency, String bicCode, String details) {
        Payment newPayment = new Payment();
        switch (paymentType) {
            case EUR_PAYMENT -> {
                if (!currency.equals(Currency.EUR)) {
                    throw new IncompatibleCurrencyException("EUR_PAYMENT has to have EUR currency.");
                }
                if (details == null || details.isEmpty()) {
                    throw new MissingFieldException("For EUR_PAYMENT details field has to be provided.");
                }
                newPayment.setDetails(details);
            }
            case USD_PAYMENT -> {
                if (!currency.equals(Currency.USD)) {
                    throw new IncompatibleCurrencyException("EUR_PAYMENT has to have USD currency.");
                }
                newPayment.setDetails(details);
            }
            case BOTH_PAYMENT -> {
                if (bicCode == null || bicCode.isEmpty()) {
                    throw new MissingFieldException("For EUR_PAYMENT bicCode field has to be provided.");
                }
                newPayment.setBicCode(bicCode);
            }
        }
        newPayment.setNotified(notifyExternalService(paymentType));
        return newPayment;
    }

    private Boolean notifyExternalService(PaymentType paymentType) {
        if (paymentType.equals(PaymentType.BOTH_PAYMENT)) {
            return null;
        }
        ResponseEntity<String> resp = restTemplate.getForEntity(paymentType.getExternalServiceAddress(), String.class);
        return resp.getStatusCode().equals(HttpStatus.OK);
    }
}
