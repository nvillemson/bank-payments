package com.project.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * table is simplified for test assignment (account name autogeneration is missing)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ACCOUNT")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BALANCE", nullable = false, columnDefinition = "DECIMAL(8,2) DEFAULT 0")
    private BigDecimal balance;

    @Column(name = "CREATED", columnDefinition = "TIMESTAMP")
    private LocalDateTime created;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID")
    private User user;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="account")
    private List<Payment> payments;
}
