package com.project.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * table is simplified for test assignment
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FIRST_NAME", nullable = false, columnDefinition = "VARCHAR(50)")
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false, columnDefinition = "VARCHAR(50)")
    private String lastName;

    @Column(name = "BIRTH_DATE", nullable = false, columnDefinition = "DATE")
    private LocalDate birthDate;

    @Column(name = "PERSONAL_ID", nullable = false, unique = true, columnDefinition = "BIGINT")
    private Long personalId;

    @Column(name = "CREATED", columnDefinition = "TIMESTAMP")
    private LocalDateTime created;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private List<Account> accounts;
}
