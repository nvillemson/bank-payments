package com.project.persistence.model;

import com.project.persistence.enums.Currency;
import com.project.persistence.enums.PaymentType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * table is simplified for test assignment
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PAYMENT")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="ACCOUNT_ID")
    private Account account;

    @Column(name = "AMOUNT", nullable = false, columnDefinition = "DECIMAL(8,2)")
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "PAYMENT_TYPE", nullable = false, columnDefinition = "VARCHAR(20)")
    private PaymentType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "CURRENCY", nullable = false, columnDefinition = "VARCHAR(20)")
    private Currency currency;

    @Column(name = "DEBITOR_IBAN", nullable = false, columnDefinition = "VARCHAR(50)")
    private String debitorIban;

    @Column(name = "CREDITOR_IBAN", nullable = false, columnDefinition = "VARCHAR(50)")
    private String creditorIban;

    @Column(name = "BIC_CODE", columnDefinition = "VARCHAR(50)")
    private String bicCode;

    @Column(name = "DETAILS", columnDefinition = "VARCHAR(150)")
    private String details;

    @Column(name = "STARTED", nullable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime started;

    @Column(name = "CANCELLED", columnDefinition = "TIMESTAMP")
    private LocalDateTime cancelled;

    @Column(name = "CANCELLATION_FEE", columnDefinition = "DECIMAL(8,2)")
    private BigDecimal cancellationFee;

    @Column(name = "NOTIFIED", columnDefinition = "BOOLEAN")
    private Boolean notified;
}
