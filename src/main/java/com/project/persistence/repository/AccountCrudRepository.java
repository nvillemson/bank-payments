package com.project.persistence.repository;

import com.project.persistence.model.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

public interface AccountCrudRepository extends CrudRepository<Account, Long> {

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query(value = "UPDATE ACCOUNT a SET a.BALANCE = a.BALANCE + :balance WHERE a.ID = :accountId AND a.USER_ID = :userId", nativeQuery = true)
    int increaseAccountBalance(@Param("userId") Long userId, @Param("accountId") Long accountId, @Param("balance") BigDecimal balance);

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query(value = "UPDATE ACCOUNT a SET a.BALANCE = a.BALANCE + :balance WHERE a.ID = :accountId", nativeQuery = true)
    int increaseAccountBalance(@Param("accountId") Long accountId, @Param("balance") BigDecimal balance);

    @Query(value = "SELECT a.BALANCE FROM ACCOUNT a WHERE a.ID = :accountId AND a.USER_ID = :userId", nativeQuery = true)
    BigDecimal findAccountBalance(@Param("userId") Long userId, @Param("accountId") Long accountId);

    @Query(value = "SELECT * FROM ACCOUNT a WHERE a.ID = :accountId AND a.USER_ID = :userId", nativeQuery = true)
    Optional<Account> findAccount(@Param("userId") Long userId, @Param("accountId") Long accountId);
}
