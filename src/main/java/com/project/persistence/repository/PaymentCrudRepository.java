package com.project.persistence.repository;

import com.project.persistence.model.Account;
import com.project.persistence.model.Payment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface PaymentCrudRepository extends CrudRepository<Payment, Long> {

    @Query(value = "SELECT * FROM PAYMENT p WHERE p.ID = :paymentId AND p.ACCOUNT_ID = :accountId", nativeQuery = true)
    Optional<Payment> findPayment(@Param("accountId") Long accountId, @Param("paymentId") Long paymentId);

    @Query(value = "SELECT p.id FROM PAYMENT p WHERE p.CANCELLED IS NULL AND p.AMOUNT >= :minAmount ORDER BY p.id LIMIT :offset, :limit", nativeQuery = true)
    List<String> findAllPaymentIds(@Param("offset") Long offset, @Param("limit") Long limit, @Param("minAmount") BigDecimal minAmount);
}
