package com.project.persistence.enums;

public enum Currency {

    EUR,
    USD
}
