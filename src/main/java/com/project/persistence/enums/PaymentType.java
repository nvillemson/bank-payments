package com.project.persistence.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * coefficient should better be properties
 * but basically PaymentType is simplified, better to be a separate table in case more currencies etc
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum PaymentType {

    EUR_PAYMENT(false, BigDecimal.valueOf(0.05), "https://www.postimees.ee/"),
    USD_PAYMENT(false, BigDecimal.valueOf(0.1), "https://rus.postimees.ee/"),
    BOTH_PAYMENT(true, BigDecimal.valueOf(0.15), null);

    private boolean bicCode;
    private BigDecimal coefficient;
    private String externalServiceAddress;

}
